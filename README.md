WIZARD
======

Wizard (short: wzrd) is a very small (43 lines, including whitespace,
include-guards, comments) header-only library that allows you to convert strings
of arbitrary length into a type whose template-parameters are the characters
of the string.

An example will probably help more than most explanation:

```c++
// taken from src/main.cpp, see there for a compiling version
const auto t = WZRD_TO_TYPE("foo bar baz");
static_assert(std::is_same<decltype(t),
              const wzrd::type_string<'f', 'o', 'o', ' ', 'b', 'a', 'r', ' ', 'b', 'a', 'z'>>::value,
              "the correct type must be inferred");
```

Two things to note:

* The macro returns a **value**, not a type. If you want the type, use `decltype`.
* The trailing `'\0'` of string-literals is ommited, because most of the time it isn't
  wanted. *If* you wanted, add it back explicitly: `"foo\0"` will keep *one* binary zero.

As you probably already guessed, this library requires C++14 (specifically: variadic, generic lambdas),
and this is a hard requirement.

License
-------

Since the library is so tiny, just do what the fuck you want ([WTFPL](https://de.wikipedia.org/wiki/WTFPL)).

Nonetheless I would consider it very nice of you, if you wouldn't delete the comment near it that states who wrote it.
