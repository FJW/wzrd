#include "wzrd/wzrd.hpp"

#include <type_traits>

int main() {
	const auto t = WZRD_TO_TYPE("foo bar baz");
	static_assert(std::is_same<decltype(t),
	              const wzrd::type_string<'f', 'o', 'o', ' ', 'b', 'a', 'r', ' ', 'b', 'a', 'z'>>::value,
	              "the correct type must be inferred");
}

