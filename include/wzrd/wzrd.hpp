#ifndef WZRD_WZRD_HPP
#define WZRD_WZRD_HPP

/**
 * Wizard (wzrd) by Florian J. Weber (oss at florianjw full stop de),
 *                                   (http(s)://florianjw.de)
 *
 * This is a tiny library that allows you to convert a string-literal
 * into a unique type.
 */


#include <cstdint>
#include <utility>


namespace wzrd {

template<char... Chars>
struct type_string{};

namespace impl {
template<typename Function, std::size_t... Indices>
auto type_string_helper(Function f, std::index_sequence<Indices...>)
-> decltype(f((std::integral_constant<std::size_t, Indices>{})...))
{
	(void) f;
	return {};
}

} // namespace impl
} // namespace wzrd


#define WZRD_TO_TYPE(str) wzrd::impl::type_string_helper(\
		[](auto... indices) {\
			return wzrd::type_string<((str)[decltype(indices)::value])...>{};\
		},\
		std::make_index_sequence<sizeof(str)-1>()\
	)

#endif // include-guard

